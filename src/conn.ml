open Bos
open Rresult
open Astring

let sudo cmd = Cmd.(v "sudo" %% cmd)
let detach name cmd = Cmd.(v "abduco" % "-n" % name %% cmd)

let log err = Format.eprintf "Error: %a\n" R.pp_msg err
let do_ur_best cmd = R.ignore_error ~use:log OS.Cmd.(in_null |> run_in cmd)
let err section line hint = R.error_msg (Format.sprintf "%s(%d): %s" section line hint)

let network = function
  | 'p' ->
    R.ignore_error ~use:log
      begin
        OS.Cmd.(in_null |> run_io Cmd.(v "ip" % "route" % "show") |> to_lines) >>= function
        | ([] | _::[] | _::_::_::_) -> err "network" __LINE__ "unexpected output of `ip route show`"
        | [route; _] ->
          R.of_option ~none:(fun () -> err "network" __LINE__ "unexpected output of `ip route show`")
            Pat.(query (v "default via $(ROUTER) dev $(DEV) proto $(PROTO) src $(SRC) metric $(METRIC)") route) >>= fun defs ->
          R.of_option ~none:(fun () -> err "network" __LINE__ "unexpected output of `ip route show`")
            (String.Map.find "ROUTER" defs) >>= fun router ->
          OS.Cmd.(in_null |> run_in Cmd.(v "ping" % "-c" % "1" % "-w" % "10" % router))
      end
  | 'P' -> List.iter (fun target -> do_ur_best Cmd.(v "ping" % "-c" % "1" % "-w" % "5" % target)) Conf.ping_targets
  | 'a' -> do_ur_best Cmd.(v "ip" % "addr" % "show")
  | 't' -> do_ur_best Cmd.(v "ssh" % "-v" % "-ND" % string_of_int Conf.sock_port % Conf.sock_host)
  | 'T' -> do_ur_best (detach "sock" Cmd.(v "ssh" % "-v" % "-ND" % string_of_int Conf.sock_port % Conf.sock_host))
  | c -> Printf.eprintf "ERROR: unknown command %c\n%!" c

let wired = function
  | 'd' -> do_ur_best (sudo Cmd.(v "dhcpcd" % "--release" % Conf.eth))
  | 'k' -> do_ur_best (sudo Cmd.(v "ip" % "link" % "set" % Conf.eth % "down"))
  | 'o' -> do_ur_best (sudo Cmd.(v "ip" % "link" % "set" % Conf.eth % "up"))
  | 'c' -> do_ur_best (sudo Cmd.(v "dhcpcd" % "--timeout" % "60" % Conf.eth))
  | 'r' -> do_ur_best (sudo Cmd.(v "dhcpcd" % "--rebind" % Conf.eth))
  | 'a' -> do_ur_best (Cmd.(v "ip" % "addr" % "show" % Conf.eth))
  | c -> network c

let wireless = function
  | 'd' ->
    R.ignore_error ~use:log
      begin
        OS.Cmd.(in_null |> run_in (sudo Cmd.(v "dhcpcd" % "--release" % Conf.wlan))) >>= fun () ->
        OS.Cmd.(in_null |> run_in (sudo Cmd.(v "pkill" % "-9" % "-u" % "root" % "wpa_supplicant")))
      end
  | 'k' ->
    R.ignore_error ~use:log
      begin
        OS.Cmd.(in_null |> run_in (sudo Cmd.(v "rfkill" % "block" % "wifi"))) >>= fun () ->
        OS.Cmd.(in_null |> run_in (sudo Cmd.(v "ip" % "link" % "set" % Conf.wlan % "down")))
      end
  | 'o' ->
    R.ignore_error ~use:log
      begin
        OS.Cmd.(in_null |> run_in (sudo Cmd.(v "rfkill" % "unblock" % "wifi"))) >>= fun () ->
        OS.Cmd.(in_null |> run_in (sudo Cmd.(v "ip" % "link" % "set" % Conf.wlan % "up")))
      end
  | 'c' ->
    R.ignore_error ~use:log
      begin
        OS.Cmd.(in_null |> run_in (sudo Cmd.(v "wpa_supplicant" % "-B" % "-Dwext" % "-i" % Conf.wlan % "-c" % Conf.wpa_conf_file))) >>= fun () ->
        OS.Cmd.(in_null |> run_in (sudo Cmd.(v "dhcpcd" % "--timeout" % "60" % Conf.wlan)))
      end
  | 's' ->
    R.ignore_error ~use:log
      begin
        OS.Cmd.(in_null |> run_in Cmd.(v "wpa_cli" % "scan")) >>= fun () ->
        OS.Cmd.(in_null |> run_in Cmd.(v "wpa_cli" % "scan_results"))
      end
  (* TODO: ('S', "scan and select", (scan networks, prompt, ask for pwd if appropriate, generate wpa_conf file *)
  | 'r' -> do_ur_best (sudo Cmd.(v "dhcpcd" % "--rebind" % Conf.wlan))
  | 'R' -> do_ur_best Cmd.(v "wpa_cli" % "reconfigure");
  | 'a' -> do_ur_best Cmd.(v "ip" % "addr" % "show" % Conf.wlan)
  | c -> network c

let dispatch mode = function
  | 'w' -> wireless
  | 'e' -> wired
  | 'n' -> network
  | c -> mode c; mode

let _: char -> unit =
  Array.fold_left
    (String.fold_left dispatch)
    wireless
    (Array.sub Sys.argv 1 (Array.length Sys.argv - 1))
