open Astring

(** Name of the ethernet interface - check `ip addr` for name of interfaces. *)
let eth = "eth0"

(** Name of the wireless interface. *)
let wlan = "wlan0"

(** Location of the wpa configuration file. *)
let wpa_conf_file = "/etc/wpa_supplicant/wpa_supplicant.conf"

(** Remote hosts to ping. *)
let ping_targets = [
  "github.com";
]

(** Hosts to connect to for socks proxy ssh tunnel. *)
let sock_port = 9876
let sock_host = "example.com"
