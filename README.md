conn — managing network connections
-------------------------------------------------------------------------------

conn offers short idiomatic interfaces to the linux cli network management
toolset.


## BUILD

### Dependencies


Build dependencies:

Binaries:
`make`,
`ocamlfind`,
`ocamlopt`.

Libraries (all available in opam):
`bos` and dependencies (`astring`, `rresult`, `fpath`).


Run dependencies:

`sudo`,
`ip`,
`ping`,
`dhcpcd`,
`pkill`,
`wpa_supplicant`,
`wpa_cli`,
`abduco` (optional),
`ssh` (optional).



### Config (optional)

```
cp src/conf.def.ml src/conf.ml
$EDITOR src/conf.ml
```

### Build

`make`


## PACKAGING

In archlinux, `makepkg` creates a pacman package.


## USAGE

`conn COMMANDS` where `COMMANDS` is a series of letters amongst:

- `w` (wifi): (default) use wireless interface
- `e` (ethernet): use wired interface
- `n` (network): be interface agnostic
- `p` (ping) (interface independent): ping the router.
- `P` (Ping) (interface independent): ping some remote hosts.
- `a` (address) (either interface): show current local configuration (IP address, routes, etc.).
- `t` (tunnel) (interface independent) (requires ssh): open socks proxy over ssh tunnel.
- `T` (Tunnel) (interface independent) (requires ssh, abduco): open socks proxy over ssh tunnel in a detached process.
- `k` (kill) (either interface): switch off selected interface (hardware switch)
- `o` (on) (either interface): switch on selected interface (hardware switch)
- `d` (disconnect) (either interface): disconnect selected interface (software only)
- `c` (connect) (either interface): connect selected interface (software only)
- `r` (reconnect) (either interface): reconnect selected interface (software only)
- `R` (Reload) (wireless only): reload configuration

Examples:

`conn dkoc` hard reset wireless connection  
`conn dkedk` disconnect and switch-off both interfaces  
`conn dkeoc` disconnect from wireless, connect ethernet

Notes:

Commands can be passed as a single argument or as a series. I.e., `conn dkoc`
can also be written `conn dk oc`, `conn d k o c`, or any other such.



