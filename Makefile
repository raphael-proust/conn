NAME=conn

all: _build/$(NAME).native

_build/$(NAME).native: build
	ocamlfind ocamlopt -package bos -linkpkg -I _build/ _build/conf.ml _build/$(NAME).ml -o _build/$(NAME).native

build: _build/ _build/$(NAME).ml _build/conf.ml

_build/:
	mkdir -p _build/

_build/%: src/%
	cp $< $@

src/conf.ml:
	cp src/conf.ml src/conf.def.ml

clean:
	rm -rf _build/
